Parse GenScot COVID19 Survey xml

Input:
  - xml in input dir

Output:
  - archivist_tables dir

Next step:
  - run [archivist_insert_pipeline](https://gitlab.com/closer-cohorts1/archivist_insert) with files from archivist_tables directory
