#!/bin/env python3

"""
    Python 3
    Parse genscot qualtrics xml file
"""

import re
import os
import xml.etree.ElementTree as ET
from collections import OrderedDict
from bs4 import BeautifulSoup

import pandas as pd
import json


main_dir = 'input'
input_name = [f for f in os.listdir(main_dir) if f.endswith('.xml')][0]
xmlFile = os.path.join(main_dir, input_name)
print(xmlFile)


# xml namespace
ns = {'r': 'ddi:reusable:3_2',
      'ddi': 'ddi:instance:3_2',
      'studyunit': 'ddi:studyunit:3_2',
      'datacollection': 'ddi:datacollection:3_2',
      'logicalproduct': 'ddi:logicalproduct:3_2',
      'group': 'ddi:group:3_2'}


def int_to_roman(num):
    """
        Convert integer to roman numeral.
    """

    roman = OrderedDict()
    roman[1000] = "m"
    roman[900] = "cm"
    roman[500] = "d"
    roman[400] = "cd"
    roman[100] = "c"
    roman[90] = "xc"
    roman[50] = "l"
    roman[40] = "xl"
    roman[10] = "x"
    roman[9] = "ix"
    roman[5] = "v"
    roman[4] = "iv"
    roman[1] = "i"

    def roman_num(num):
        for r in roman.keys():
            x, y = divmod(num, r)
            yield roman[r] * x
            num -= (r * x)
            if num <= 0:
                break
    if num == 0:
        return "0"
    else:
        return "".join([a for a in roman_num(num)])


def clean_Literal(Literal):
    """
    input: literal from question/statement
    output: cleaned up version
    """
    if not Literal is None:
        # first step: get text from html element
        soup = BeautifulSoup(''.join(Literal), "html.parser")

        if Literal.startswith('<div>'):
            Literal_text_1 = soup.find('div').text
        if Literal.startswith('<p>'):
            Literal_text_1 = soup.find('p').text
        if Literal.startswith('<span>'):
            Literal_text_1 = soup.find('span').text
        else:
            Literal_text_1 = soup.text

        # second step: get text from Literal_text_1
        soup_2 = BeautifulSoup(''.join(Literal_text_1), "html.parser")
        if Literal_text_1.startswith('<div>'):
            Literal_text = soup_2.find('div').text
        if Literal_text_1.startswith('<p>'):
            Literal_text = soup_2.find('p').text
        if Literal_text_1.startswith('<span>'):
            Literal_text = soup_2.find('span').text
        else:
            Literal_text = soup_2.text
    else:
        Literal_text = ''

    return Literal_text.replace('\t', ' ').replace('\n', ' ')


def parse_InstrumentScheme(root):
    """
    input: root of ET xml
    output: InstrumentScheme dict
    """
    InstrumentScheme_dict = {}
    InstrumentScheme = root.find('./ddi:Fragment/datacollection:InstrumentScheme', ns)

    if not InstrumentScheme is None:
        InstrumentScheme_dict['URN'] = InstrumentScheme.find('r:URN', ns).text
        InstrumentScheme_dict['Agency'] = InstrumentScheme.find('r:Agency', ns).text
        InstrumentScheme_dict['ID'] = InstrumentScheme.find('r:ID', ns).text
        InstrumentScheme_dict['Version'] = InstrumentScheme.find('r:Version', ns).text
        InstrumentScheme_dict['Label'] = InstrumentScheme.find('r:Label/r:Content', ns).text

        InstrumentReference = InstrumentScheme.find('datacollection:InstrumentReference', ns)
        if not InstrumentReference is None:
            InstrumentReference_dict = {}
            InstrumentReference_dict['Agency'] = InstrumentReference.find('r:Agency', ns).text
            InstrumentReference_dict['ID'] = InstrumentReference.find('r:ID', ns).text
            InstrumentReference_dict['Version'] = InstrumentReference.find('r:Version', ns).text
            InstrumentReference_dict['TypeOfObject'] = InstrumentReference.find('r:TypeOfObject', ns).text

        InstrumentScheme_dict['InstrumentReference'] = InstrumentReference_dict

    return InstrumentScheme_dict


def parse_QuestionScheme (root):
    """
    input: root of ET xml
    output: QuestionScheme dict
    """
    QuestionScheme_dict = {}
    QuestionScheme = root.find('./ddi:Fragment/datacollection:QuestionScheme', ns)
    if not QuestionScheme is None:
        QuestionScheme_dict['URN'] = QuestionScheme.find('r:URN', ns).text
        QuestionScheme_dict['Agency'] = QuestionScheme.find('r:Agency', ns).text
        QuestionScheme_dict['ID'] = QuestionScheme.find('r:ID', ns).text
        QuestionScheme_dict['Version'] = QuestionScheme.find('r:Version', ns).text
        if not QuestionScheme.find('datacollection:QuestionSchemeName/r:String', ns) is None:
            QuestionScheme_dict['Name'] = QuestionScheme.find('datacollection:QuestionSchemeName/r:String', ns).text
        if not QuestionScheme.find('r:Label/r:Content', ns) is None:
            QuestionScheme_dict['Label'] = QuestionScheme.find('r:Label/r:Content', ns).text

        QuestionItemReference_list = []
        QuestionItemReference_all = QuestionScheme.findall('datacollection:QuestionItemReference', ns)

        if not QuestionItemReference_all is None:
            for x, QuestionItemReference in enumerate(QuestionItemReference_all):
                QuestionItemReference_dict = {}
                QuestionItemReference_dict['QuestionItemReference_index'] = x
                QuestionItemReference_dict['Agency'] = QuestionItemReference.find('r:Agency', ns).text
                QuestionItemReference_dict['ID'] = QuestionItemReference.find('r:ID', ns).text
                QuestionItemReference_dict['Version'] = QuestionItemReference.find('r:Version', ns).text
                QuestionItemReference_dict['TypeOfObject'] = QuestionItemReference.find('r:TypeOfObject', ns).text

                QuestionItemReference_list.append(QuestionItemReference_dict)

        QuestionScheme_dict['QuestionItemReference'] = QuestionItemReference_list

        QuestionGroupReference_list = []
        QuestionGroupReference_all = QuestionScheme.findall('datacollection:QuestionGroupReference', ns)

        if not QuestionGroupReference_all is None:
            for x, QuestionGroupReference in enumerate(QuestionGroupReference_all):
                QuestionGroupReference_dict = {}
                QuestionGroupReference_dict['QuestionGroupReference_index'] = x
                QuestionGroupReference_dict['Agency'] = QuestionGroupReference.find('r:Agency', ns).text
                QuestionGroupReference_dict['ID'] = QuestionGroupReference.find('r:ID', ns).text
                QuestionGroupReference_dict['Version'] = QuestionGroupReference.find('r:Version', ns).text
                QuestionGroupReference_dict['TypeOfObject'] = QuestionGroupReference.find('r:TypeOfObject', ns).text

                QuestionGroupReference_list.append(QuestionGroupReference_dict)

        QuestionScheme_dict['QuestionGroupReference'] = QuestionGroupReference_list

    return QuestionScheme_dict


def parse_CategoryScheme(root):
    """
    input: root of ET xml
    output: CategoryScheme list of dict
    """
    CategoryScheme_list = []
    CategoryScheme_all = root.findall('./ddi:Fragment/logicalproduct:CategoryScheme', ns)

    for x, CategoryScheme in enumerate(CategoryScheme_all):
        CategoryScheme_dict = {}
        CategoryScheme_dict['CategoryScheme_index'] = x
        CategoryScheme_dict['URN'] = CategoryScheme.find('r:URN', ns).text
        CategoryScheme_dict['Agency'] = CategoryScheme.find('r:Agency', ns).text
        CategoryScheme_dict['ID'] = CategoryScheme.find('r:ID', ns).text
        CategoryScheme_dict['Version'] = CategoryScheme.find('r:Version', ns).text
        CategoryScheme_dict['Label'] = CategoryScheme.find('r:Label/r:Content', ns).text

        CategoryReference = CategoryScheme.find('r:CategoryReference', ns)
        if not CategoryReference is None:
            CategoryReference_dict = {}
            CategoryReference_dict['Agency'] = CategoryReference.find('r:Agency', ns).text
            CategoryReference_dict['ID'] = CategoryReference.find('r:ID', ns).text
            CategoryReference_dict['Version'] = CategoryReference.find('r:Version', ns).text
            CategoryReference_dict['TypeOfObject'] = CategoryReference.find('r:TypeOfObject', ns).text

        CategoryScheme_dict['CategoryReference'] = CategoryReference_dict

        CategoryScheme_list.append(CategoryScheme_dict)

    return CategoryScheme_list


def parse_ControlConstructScheme(root):
    """
    input: root of ET xml
    output: ControlConstructScheme dict
    """
    ControlConstructScheme_dict = {}
    ControlConstructScheme = root.find('./ddi:Fragment/datacollection:ControlConstructScheme', ns)
    if not ControlConstructScheme is None:
        ControlConstructScheme_dict['URN'] = ControlConstructScheme.find('r:URN', ns).text
        ControlConstructScheme_dict['Agency'] = ControlConstructScheme.find('r:Agency', ns).text
        ControlConstructScheme_dict['ID'] = ControlConstructScheme.find('r:ID', ns).text
        ControlConstructScheme_dict['Version'] = ControlConstructScheme.find('r:Version', ns).text
        ControlConstructScheme_dict['Label'] = ControlConstructScheme.find('r:Label/r:Content', ns).text
        ControlConstructReference_list = []
        ControlConstructReference_all = ControlConstructScheme.findall('datacollection:ControlConstructReference', ns)

        if not ControlConstructReference_all is None:
            for x, ControlConstructReference in enumerate(ControlConstructReference_all):
                ControlConstructReference_dict = {}
                ControlConstructReference_dict['ControlConstructReference_index'] = x
                ControlConstructReference_dict['Agency'] = ControlConstructReference.find('r:Agency', ns).text
                ControlConstructReference_dict['ID'] = ControlConstructReference.find('r:ID', ns).text
                ControlConstructReference_dict['Version'] = ControlConstructReference.find('r:Version', ns).text
                ControlConstructReference_dict['TypeOfObject'] = ControlConstructReference.find('r:TypeOfObject', ns).text

                ControlConstructReference_list.append(ControlConstructReference_dict)

        ControlConstructScheme_dict['ControlConstructReference'] = ControlConstructReference_list

    return ControlConstructScheme_dict


def parse_QuestionConstruct(root):
    """
    input: root of ET xml
    output: QuestionConstruct list of dict
    """
    QuestionConstruct_list = []
    QuestionConstruct_all = root.findall('./ddi:Fragment/datacollection:QuestionConstruct', ns)

    for x, QuestionConstruct in enumerate(QuestionConstruct_all):
        QuestionConstruct_dict = {}
        QuestionConstruct_dict['QuestionConstruct_index'] = x
        QuestionConstruct_dict['URN'] = QuestionConstruct.find('r:URN', ns).text
        QuestionConstruct_dict['Agency'] = QuestionConstruct.find('r:Agency', ns).text
        QuestionConstruct_dict['ID'] = QuestionConstruct.find('r:ID', ns).text
        QuestionConstruct_dict['Version'] = QuestionConstruct.find('r:Version', ns).text

        if QuestionConstruct.find('datacollection:ConstructName/r:String', ns) is None:
            QuestionConstruct_dict['ConstructName'] = str(x)
        else:
            QuestionConstruct_dict['ConstructName'] = QuestionConstruct.find('datacollection:ConstructName/r:String', ns).text

        QuestionReference = QuestionConstruct.find('r:QuestionReference', ns)
        if not QuestionReference is None:
            QuestionReference_dict = {}
            QuestionReference_dict['Agency'] = QuestionReference.find('r:Agency', ns).text
            QuestionReference_dict['ID'] = QuestionReference.find('r:ID', ns).text
            QuestionReference_dict['Version'] = QuestionReference.find('r:Version', ns).text
            QuestionReference_dict['TypeOfObject'] = QuestionReference.find('r:TypeOfObject', ns).text

        QuestionConstruct_dict['QuestionReference'] = QuestionReference_dict

        QuestionConstruct_list.append(QuestionConstruct_dict)

    return QuestionConstruct_list


def parse_ResourcePackage(root):
    """
    input: root of ET xml
    output: ResourcePackage dict
    """
    ResourcePackage_dict = {}
    ResourcePackage = root.find('./ddi:Fragment/group:ResourcePackage', ns)
    if not ResourcePackage is None:
        ResourcePackage_dict['URN'] = ResourcePackage.find('r:URN', ns).text
        ResourcePackage_dict['Agency'] = ResourcePackage.find('r:Agency', ns).text
        ResourcePackage_dict['ID'] = ResourcePackage.find('r:ID', ns).text
        ResourcePackage_dict['Version'] = ResourcePackage.find('r:Version', ns).text
        UserAttributePair = ResourcePackage.find('r:UserAttributePair', ns)
        if not UserAttributePair is None:
            UserAttributePair_key = UserAttributePair.find('r:AttributeKey', ns).text
            UserAttributePair_value = UserAttributePair.find('r:AttributeValue', ns).text
        ResourcePackage_dict[UserAttributePair_key] = UserAttributePair_value
        ResourcePackage_dict['Title'] = ResourcePackage.find('r:Citation/r:Title/r:String', ns).text

        #TODO: how to preserve the order for the rest of ref?

    return ResourcePackage_dict


def parse_Instrument(root):
    """
    input: root of ET xml
    output: Instrument dict
    """
    Instrument_dict = {}
    Instrument = root.find('./ddi:Fragment/datacollection:Instrument', ns)

    if not Instrument is None:
        Instrument_dict['URN'] = Instrument.find('r:URN', ns).text
        Instrument_dict['Agency'] = Instrument.find('r:Agency', ns).text
        Instrument_dict['ID'] = Instrument.find('r:ID', ns).text
        Instrument_dict['Version'] = Instrument.find('r:Version', ns).text
        Instrument_dict['Label'] = Instrument.find('r:Label/r:Content', ns).text

        ControlConstructReference = Instrument.find('datacollection:ControlConstructReference', ns)
        if not ControlConstructReference is None:
            ControlConstructReference_dict = {}
            ControlConstructReference_dict['Agency'] = ControlConstructReference.find('r:Agency', ns).text
            ControlConstructReference_dict['ID'] = ControlConstructReference.find('r:ID', ns).text
            ControlConstructReference_dict['Version'] = ControlConstructReference.find('r:Version', ns).text
            ControlConstructReference_dict['TypeOfObject'] = ControlConstructReference.find('r:TypeOfObject', ns).text

        Instrument_dict['InstrumentReference'] = ControlConstructReference_dict

    return Instrument_dict


def parse_Sequence(root):
    """
    input: root of ET xml
    output: Sequence list of list
    """
    Sequence_list = []
    Sequence_all = root.findall('./ddi:Fragment/datacollection:Sequence', ns)

    for x, Sequence in enumerate(Sequence_all):
        Sequence_dict = {}
        Sequence_dict['Sequence_index'] = x
        Sequence_dict['URN'] = Sequence.find('r:URN', ns).text
        Sequence_dict['Agency'] = Sequence.find('r:Agency', ns).text
        Sequence_dict['ID'] = Sequence.find('r:ID', ns).text
        Sequence_dict['Version'] = Sequence.find('r:Version', ns).text
        if Sequence.find('r:Label/r:Content', ns) is None:
            Sequence_dict['Label'] = None
        else:
            Sequence_dict['Label'] = Sequence.find('r:Label/r:Content', ns).text

        ControlConstructReference_list = []
        ControlConstructReference_all = Sequence.findall('datacollection:ControlConstructReference', ns)
        if not ControlConstructReference_all is None:
            for y, ControlConstructReference in enumerate(ControlConstructReference_all):
                ControlConstructReference_dict = {}
                ControlConstructReference_dict['ControlConstructReference_index'] = y
                ControlConstructReference_dict['Agency'] = ControlConstructReference.find('r:Agency', ns).text
                ControlConstructReference_dict['ID'] = ControlConstructReference.find('r:ID', ns).text
                ControlConstructReference_dict['Version'] = ControlConstructReference.find('r:Version', ns).text
                ControlConstructReference_dict['TypeOfObject'] = ControlConstructReference.find('r:TypeOfObject', ns).text
                ControlConstructReference_list.append(ControlConstructReference_dict)

            Sequence_dict['ControlConstructReference'] = ControlConstructReference_list

        if Sequence.find('datacollection:ConstructSequence/datacollection:ItemSequenceType', ns) is None:
            Sequence_dict['ItemSequenceType'] = None
        else:
            Sequence_dict['ItemSequenceType'] = Sequence.find('datacollection:ConstructSequence/datacollection:ItemSequenceType', ns).text

        Sequence_list.append(Sequence_dict)

    return Sequence_list


def parse_StatementItem(root):
    """
    input: root of ET xml
    output: StatementItem list of dict
    """
    StatementItem_list = []
    StatementItem_all = root.findall('./ddi:Fragment/datacollection:StatementItem', ns)

    for x, StatementItem in enumerate(StatementItem_all):

        StatementItem_dict = {}
        StatementItem_dict['StatementItem_index'] = x
        StatementItem_dict['URN'] = StatementItem.find('r:URN', ns).text
        StatementItem_dict['Agency'] = StatementItem.find('r:Agency', ns).text
        StatementItem_dict['ID'] = StatementItem.find('r:ID', ns).text
        StatementItem_dict['Version'] = StatementItem.find('r:Version', ns).text
        UserAttributePair = StatementItem.find('r:UserAttributePair', ns)
        if not UserAttributePair is None:
            UserAttributePair_key = UserAttributePair.find('r:AttributeKey', ns).text
            UserAttributePair_value = UserAttributePair.find('r:AttributeValue', ns).text
        StatementItem_dict[UserAttributePair_key] = UserAttributePair_value
        Literal = StatementItem.find('datacollection:DisplayText/datacollection:LiteralText/datacollection:Text', ns)
        if not Literal is None:
            StatementItem_dict['Literal'] = Literal.text
        else:
            StatementItem_dict['Literal'] = None

        StatementItem_list.append(StatementItem_dict)

    return StatementItem_list


def parse_QuestionGroup(root):
    """
    input: root of ET xml
    output: QuestionGroup list of dict
    """
    QuestionGroup_list = []
    QuestionGroup_all = root.findall('./ddi:Fragment/datacollection:QuestionGroup', ns)

    for x, QuestionGroup in enumerate(QuestionGroup_all):

        QuestionGroup_dict = {}

        QuestionGroup_dict['QuestionGroup_index'] = x
        QuestionGroup_dict['URN'] = QuestionGroup.find('r:URN', ns).text
        QuestionGroup_dict['Agency'] = QuestionGroup.find('r:Agency', ns).text
        QuestionGroup_dict['ID'] = QuestionGroup.find('r:ID', ns).text
        QuestionGroup_dict['Version'] = QuestionGroup.find('r:Version', ns).text
        UserAttributePair = QuestionGroup.find('r:UserAttributePair', ns)
        if not UserAttributePair is None:
            UserAttributePair_key = UserAttributePair.find('r:AttributeKey', ns).text
            UserAttributePair_value = UserAttributePair.find('r:AttributeValue', ns).text
        QuestionGroup_dict[UserAttributePair_key] = UserAttributePair_value

        QuestionGroup_dict['Label'] = QuestionGroup.find('r:Label/r:Content', ns).text
        QuestionItemReference_list = []
        QuestionItemReference_all = QuestionGroup.findall('datacollection:QuestionItemReference', ns)
        for i, QuestionItemReference in enumerate(QuestionItemReference_all):
            QuestionItemReference_dict = {}
            QuestionItemReference_dict['QuestionItemReferenc_index'] = i
            QuestionItemReference_dict['Agency'] = QuestionItemReference.find('r:Agency', ns).text
            QuestionItemReference_dict['ID'] = QuestionItemReference.find('r:ID', ns).text
            QuestionItemReference_dict['Version'] = QuestionItemReference.find('r:Version', ns).text
            QuestionItemReference_dict['TypeOfObject'] = QuestionItemReference.find('r:TypeOfObject', ns).text
            QuestionItemReference_list.append(QuestionItemReference_dict)
        QuestionGroup_dict['QuestionItemReference'] = QuestionItemReference_list

        QuestionGroup_list.append(QuestionGroup_dict)

    return QuestionGroup_list


def parse_QuestionItem(root):
    """
    input: root of ET xml
    output: QuestionItem list of dict
    """
    QuestionItem_list = []
    QuestionItem_all = root.findall('./ddi:Fragment/datacollection:QuestionItem', ns)

    for x, QuestionItem in enumerate(QuestionItem_all):
        QuestionItem_dict = {}
        QuestionItem_dict['QuestionItem_index'] = x
        QuestionItem_dict['URN'] = QuestionItem.find('r:URN', ns).text
        QuestionItem_dict['Agency'] = QuestionItem.find('r:Agency', ns).text
        QuestionItem_dict['ID'] = QuestionItem.find('r:ID', ns).text
        QuestionItem_dict['Version'] = QuestionItem.find('r:Version', ns).text
        QuestionItem_dict['Question_ID'] = QuestionItem.find('datacollection:QuestionItemName/r:String', ns).text
        QuestionItem_dict['Literal'] = QuestionItem.find('datacollection:QuestionText/datacollection:LiteralText/datacollection:Text', ns).text

        # UserAttributePair and question instruction
        Instructions = None
        Name = None
        UserAttributePair_list = []
        UserAttributePairs = QuestionItem.findall('r:UserAttributePair', ns)
        for UserAttributePair in UserAttributePairs:
            UserAttributePair_dict = {}
            if not UserAttributePair is None:
                UserAttributePair_key = UserAttributePair.find('r:AttributeKey', ns).text
                UserAttributePair_value = UserAttributePair.find('r:AttributeValue', ns).text
                UserAttributePair_dict[UserAttributePair_key] = UserAttributePair_value
                UserAttributePair_list.append(UserAttributePair_dict)
                if UserAttributePair_key == 'extension:QuestionInstruction':
                    UserAttributePair_value = json.loads(UserAttributePair_value)
                    Instructions = list(UserAttributePair_value.values())[0]
                if UserAttributePair_key == 'Qualtrics:DataExportTag':
                    Name = UserAttributePair_value
        QuestionItem_dict['UserAttributePair'] = UserAttributePair_list
        QuestionItem_dict['Instructions'] = Instructions
        if Name is not None:
            QuestionItem_dict['Name'] = Name
        else:
            QuestionItem_dict['Name'] = QuestionItem_dict['Question_ID']

        ResponseCardinality = QuestionItem.find('r:ResponseCardinality', ns)
        QuestionItem_dict['minimumResponses'] = ResponseCardinality.attrib['minimumResponses']
        QuestionItem_dict['maximumResponses'] = ResponseCardinality.attrib['maximumResponses']

        TextDomain_dict = {}
        TextDomain = QuestionItem.find('datacollection:TextDomain', ns)
        if not TextDomain is None:
            # label
            if TextDomain.find('r:Label', ns) is None:
                TextDomain_dict['Label'] = 'Generic text'
            else:
                TextDomain_dict['Label'] = TextDomain.find('r:Label/r:Content', ns).text

            if 'minLength' in TextDomain.attrib.keys():
                TextDomain_dict['minLength'] = TextDomain.attrib['minLength']
            else:
                TextDomain_dict['minLength'] = None

            if 'maxLength' in TextDomain.attrib.keys():
                TextDomain_dict['maxLength'] = TextDomain.attrib['maxLength']
            else:
                TextDomain_dict['maxLength'] = None
        QuestionItem_dict['TextDomain'] = TextDomain_dict

        NumericDomain_dict = {}
        NumericDomain = QuestionItem.find('datacollection:NumericDomain', ns)
        if not NumericDomain is None:
            if NumericDomain.find('r:Label', ns) == None:
                NumericDomain_dict['Label'] = 'Generic number'
            else:
                NumericDomain_dict['Label'] = NumericDomain.find('r:Label/r:Content', ns).text

            if NumericDomain.find('r:NumericTypeCode', ns) == None:
                NumericDomain_dict['NumericType'] = None
            else:
                NumericDomain_dict['NumericType'] = NumericDomain.find('r:NumericTypeCode', ns).text

            if NumericDomain.find('r:NumberRange/r:Low', ns) == None:
                NumericDomain_dict['low_value'] = None
            else:
                NumericDomain_dict['low_value'] = NumericDomain.find('r:NumberRange/r:Low', ns).text

            if NumericDomain.find('r:NumberRange/r:High', ns) == None:
                NumericDomain_dict['high_value'] = None
            else:
                NumericDomain_dict['high_value'] = NumericDomain.find('r:NumberRange/r:High', ns).text
        QuestionItem_dict['NumericDomain'] = NumericDomain_dict

        DateTimeDomain_dict = {}
        DateTimeDomain = QuestionItem.find('datacollection:DateTimeDomain', ns)
        if not DateTimeDomain is None:
            if DateTimeDomain.find('r:Label', ns) == None:
                DateTimeDomain_dict['Format'] = None
            else:
                DateTimeDomain_dict['Format'] = DateTimeDomain.find('r:Label/r:Content', ns).text

            if DateTimeDomain.find('r:DateTypeCode', ns) == None:
                DateTimeDomain_dict['DateType'] = None
            else:
                DateTimeDomain_dict['DateType'] = DateTimeDomain.find('r:DateTypeCode', ns).text
        QuestionItem_dict['DateTimeDomain'] = DateTimeDomain_dict

        CodeDomain_dict = {}
        CodeDomain = QuestionItem.find('datacollection:CodeDomain', ns)
        if not CodeDomain is None:
            CodeListReference_dict = {}
            CodeListReference = CodeDomain.find('r:CodeListReference', ns)
            CodeListReference_dict['Agency'] = CodeListReference.find('r:Agency', ns).text
            CodeListReference_dict['ID'] = CodeListReference.find('r:ID', ns).text
            CodeListReference_dict['Version'] = CodeListReference.find('r:Version', ns).text
            CodeListReference_dict['TypeOfObject'] = CodeListReference.find('r:TypeOfObject', ns).text
            CodeDomain_dict['CodeListReference'] = CodeListReference_dict

            ResponseCardinality_dict = {}
            ResponseCardinality = CodeDomain.find('r:ResponseCardinality', ns)
            ResponseCardinality_dict['minimumResponses'] = ResponseCardinality.attrib['minimumResponses']
            if 'maximumResponses' in ResponseCardinality.attrib.keys():
                ResponseCardinality_dict['maximumResponses'] = ResponseCardinality.attrib['maximumResponses']
            else:
                ResponseCardinality_dict['maximumResponses'] = None

            CodeDomain_dict['ResponseCardinality'] =  ResponseCardinality_dict
        QuestionItem_dict['CodeDomain'] = CodeDomain_dict

        QuestionItem_list.append(QuestionItem_dict)

    return QuestionItem_list


def parse_CodeList(root):
    """
    input: root of ET xml
    output: CodeList, list of list
    """
    CodeList_list = []
    CodeList_all = root.findall('./ddi:Fragment/logicalproduct:CodeList', ns)

    for x, CodeList in enumerate(CodeList_all):
        CodeList_dict = {}
        CodeList_dict['CodeList_index'] = x
        CodeList_dict['URN'] = CodeList.find('r:URN', ns).text
        CodeList_dict['Agency'] = CodeList.find('r:Agency', ns).text
        CodeList_dict['ID'] = CodeList.find('r:ID', ns).text
        CodeList_dict['Version'] = CodeList.find('r:Version', ns).text
        if CodeList.find('r:Label/r:Content', ns) is None:
            CodeList_dict['Label'] = None
        else:
            CodeList_dict['Label'] = CodeList.find('r:Label/r:Content', ns).text

        Code_list = []
        Code_all = CodeList.findall('logicalproduct:Code', ns)
        if not Code_all is None:
            for y, Code in enumerate(Code_all):
                Code_dict = {}
                Code_dict['Code_index'] = y 
                Code_dict['URN'] = Code.find('r:URN', ns).text
                Code_dict['Agency'] = Code.find('r:Agency', ns).text
                Code_dict['ID'] = Code.find('r:ID', ns).text
                Code_dict['Version'] = Code.find('r:Version', ns).text

                CategoryReference_dict = {}
                CategoryReference = Code.find('r:CategoryReference', ns)
                if not CategoryReference is None:
                    CategoryReference_dict['Agency'] = CategoryReference.find('r:Agency', ns).text
                    CategoryReference_dict['ID'] = CategoryReference.find('r:ID', ns).text
                    CategoryReference_dict['Version'] = CategoryReference.find('r:Version', ns).text
                    CategoryReference_dict['TypeOfObject'] = CategoryReference.find('r:TypeOfObject', ns).text
                Code_dict['CategoryReference'] = CategoryReference_dict

                Code_dict['Value'] = Code.find('r:Value', ns).text
                Code_list.append(Code_dict)

            CodeList_dict['Code'] = Code_list

        CodeList_list.append(CodeList_dict)

    return CodeList_list


def parse_Category(root):
    """
    input: root of ET xml
    output: Category list of dict
    """
    Category_list = []
    Category_all = root.findall('./ddi:Fragment/logicalproduct:Category', ns)

    for x, Category in enumerate(Category_all):
        Category_dict = {}
        Category_dict['Category_index'] = x
        Category_dict['URN'] = Category.find('r:URN', ns).text
        Category_dict['Agency'] = Category.find('r:Agency', ns).text
        Category_dict['ID'] = Category.find('r:ID', ns).text
        Category_dict['Version'] = Category.find('r:Version', ns).text

        if Category.find('r:Label/r:Content', ns) is None:
            Category_dict['Label'] = None
        else:
            Category_dict['Label'] = Category.find('r:Label/r:Content', ns).text

        Category_list.append(Category_dict)

    return Category_list


def parse_IfThenElse(root):
    """
    input: root of ET xml
    output: IfThenElse list of dict
    """
    IfThenElse_list = []
    IfThenElse_all = root.findall('./ddi:Fragment/datacollection:IfThenElse', ns)

    for x, IfThenElse in enumerate(IfThenElse_all):
        IfThenElse_dict = {}
        IfThenElse_dict['IfThenElse_index'] = x
        IfThenElse_dict['URN'] = IfThenElse.find('r:URN', ns).text
        IfThenElse_dict['Agency'] = IfThenElse.find('r:Agency', ns).text
        IfThenElse_dict['ID'] = IfThenElse.find('r:ID', ns).text
        IfThenElse_dict['Version'] = IfThenElse.find('r:Version', ns).text

        IfCondition_dict = {}
        IfCondition = IfThenElse.find('datacollection:IfCondition', ns)
        if not IfCondition is None:
            IfCondition_dict['ProgramLanguage'] = IfCondition.find('r:Command/r:ProgramLanguage', ns).text
            IfCondition_dict['CommandContent'] = IfCondition.find('r:Command/r:CommandContent', ns).text
        IfThenElse_dict['IfCondition'] = IfCondition_dict

        ThenConstructReference_dict = {}
        ThenConstructReference = IfThenElse.find('datacollection:ThenConstructReference', ns)
        if not ThenConstructReference_dict is None:
            ThenConstructReference_dict['Agency'] = ThenConstructReference.find('r:Agency', ns).text
            ThenConstructReference_dict['ID'] = ThenConstructReference.find('r:ID', ns).text
            ThenConstructReference_dict['Version'] = ThenConstructReference.find('r:Version', ns).text
            ThenConstructReference_dict['TypeOfObject'] = ThenConstructReference.find('r:TypeOfObject', ns).text
        IfThenElse_dict['ThenConstructReference'] = ThenConstructReference_dict

        IfThenElse_list.append(IfThenElse_dict)

    return IfThenElse_list


def parse_TopLevelReference(root):
    """
    input: root of ET xml
    output: TopLevelReference dict
    """
    TopLevelReference_dict = {}

    TopLevelReference = root.find('./ddi:TopLevelReference', ns)
    TopLevelReference_dict['Agency'] = TopLevelReference.find('r:Agency', ns).text
    TopLevelReference_dict['ID'] = TopLevelReference.find('r:ID', ns).text
    TopLevelReference_dict['Version'] = TopLevelReference.find('r:Version', ns).text
    TopLevelReference_dict['TypeOfObject'] = TopLevelReference.find('r:TypeOfObject', ns).text

    return TopLevelReference_dict


def get_sequence_label(root):
    """
    add new label to parsed sequence
    """
    Sequence_list = parse_Sequence(root)
    label_list = []
    for Sequence in Sequence_list:
        seq_label = Sequence['Label']

        if not seq_label is None:
            # seq_label = seq_label.split('DataDictionary')[0]
            # number of occurrences
            num_o = label_list.count(seq_label)

            if num_o > 0:
                new_label = seq_label + ' duplicate ' + str(num_o)
            else:
                new_label = seq_label

            #print(seq_label, new_label)
            Sequence['new_label'] = new_label
        else:
            Sequence['new_label'] = os.path.splitext(input_name)[0]
        label_list.append(seq_label)

    return Sequence_list


def get_sequence_table(root):
    """
    input: using 'InstrumentScheme', 'Instrument', 'Sequence'
    output: sequence table
    """
    # 'InstrumentScheme' point to 'Instrument'
    # could start from 'Instrument': only one 'Instrument'
    Instrument_dict = parse_Instrument(root)

    if Instrument_dict['InstrumentReference']['TypeOfObject'] == 'Sequence':
        top_Seq_ID = Instrument_dict['InstrumentReference']['ID']

    Sequence_list = get_sequence_label(root)
    """
    # missing some sequences from Sequence_list[0]
    seq_list = []
    seq_label_dict = {}
    for Sequence in Sequence_list:
        # build ID, Label dictionary
        seq_label_dict[Sequence['ID']] = Sequence['new_label']

        # for each ControlConstructReference, find reference type
        for item in Sequence['ControlConstructReference']:
            if item['TypeOfObject'] == 'Sequence':
                seq_parent_dict = {}
                seq_parent_dict['ID'] = item['ID']
                seq_parent_dict['Position'] = item['ControlConstructReference_index']
                seq_parent_dict['Parent_ID'] = Sequence['ID']

                seq_list.append(seq_parent_dict)

    # build table
    df = pd.DataFrame(seq_list)
    # re-map from label dictionary
    df = df.replace({'ID': seq_label_dict, 'Parent_ID': seq_label_dict})
    # rename
    df = df.rename(columns={'ID': 'Label', 'Parent_ID': 'Parent_Name'})
    df['Parent_Type'] = 'CcSequence'
    df['Branch'] = None
    """

    # first chunk is same as from Sequence_list[0], later sequences are out of order, one could manually modify sequences table later
    keys_to_extract= ['Sequence_index', 'Label']
    seq_list = []
    seq_label_dict = {}
    for Sequence in Sequence_list:
        # build ID, Label dictionary
        seq_label_dict[Sequence['ID']] = Sequence['new_label']
        subset_dict = {key: Sequence[key] for key in keys_to_extract}
        if subset_dict['Label'] is not None:
            seq_list.append(subset_dict)

    # build table
    df = pd.DataFrame(seq_list)
    df['Position'] =  df.index + 1
    df['Parent_Type'] = 'CcSequence'
    df['Parent_Name'] = seq_label_dict[top_Seq_ID]
    df['Branch'] = None

    # re-order columns
    cols = ['Label', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df = df[cols]

    # quick fix
    df['Parent_Name'] = df['Parent_Name'].apply(lambda x: seq_label_dict[top_Seq_ID]if x is None else x)

    # insert top level row
    df.loc[-1] = [seq_label_dict[top_Seq_ID], 'CcSequence', None, None, 1]  # adding a row
    df.index = df.index + 1  # shifting index
    df.sort_index(inplace=True)

    return df


def get_position_w_seq(root):
    """
    find parent relationship and position with Sequence
    """

    Sequence_list = get_sequence_label(root)

    seq_list = []
    seq_label_dict = {}
    for Sequence in Sequence_list:
        # build ID, Label dictionary
        seq_label_dict[Sequence['ID']] = Sequence['new_label']

        # for each ControlConstructReference, find reference type
        for item in Sequence['ControlConstructReference']:
                seq_parent_dict = {}
                seq_parent_dict['ID'] = item['ID']
                seq_parent_dict['TypeOfObject'] = item['TypeOfObject']
                seq_parent_dict['Position'] = item['ControlConstructReference_index'] + 1
                seq_parent_dict['Parent_Seq_ID'] = Sequence['ID']
                seq_parent_dict['Parent_Seq_Label'] = Sequence['new_label']

                seq_list.append(seq_parent_dict)
    return seq_list


def get_statement_table(root):
    """
    input: parsed 'statement' and 'sequence'
    output: statement table
    """
    position_list = get_position_w_seq(root)
    seq_position_list = [item for item in position_list if item['TypeOfObject'] == 'StatementItem']

    statement_list = parse_StatementItem(root)

    state_list = []
    for statement in statement_list:
        state_dict = {}
        state_dict['Label'] = 's_q' + str(statement['StatementItem_index'] + 1)
        state_dict['Literal'] = clean_Literal(statement['Literal'])

        parent_seq_list = [item for item in seq_position_list if item['ID'] == statement['ID']]
        if len(parent_seq_list) == 1:
            parent_seq = parent_seq_list[0]
            state_dict['Parent_Type'] = 'CcSequence'
            state_dict['Parent_Name'] = parent_seq['Parent_Seq_Label']
            state_dict['Position'] = parent_seq['Position']

        state_list.append(state_dict)
    # build table
    df = pd.DataFrame(state_list)
    df['Branch'] = None
    cols = ['Label', 'Literal', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    if not df.empty:
        df = df[cols]
    else:
        df= pd.DataFrame(columns=cols)

    return df


def get_codelist_table(root):
    """
    input: parsed 'codelist', ''
    output: codelist table, codelist ID and codelist label dictionary
    """

    CodeList_list = parse_CodeList(root)
    Category_list = parse_Category(root)

    code_list = []
    codelist_dict = {}
    for CodeList in CodeList_list:
        code_dict = {}
        # CodeList['Label'] is too long, using index instead
        code_label = 'cs_' + str(CodeList['CodeList_index'] + 1)
        codelist_dict[CodeList['ID']] = code_label

        for Code in CodeList['Code']:
            code_dict = {}
            code_dict['Label'] = code_label
            code_dict['Code_Order'] = Code['Code_index'] + 1
            code_dict['Code_Value'] = Code['Value']

            cat_list = [item for item in Category_list if item['ID'] == Code['CategoryReference']['ID']]
            if len(cat_list) == 1:
                cat = cat_list[0]
                code_dict['Category'] = cat['Label']
            code_list.append(code_dict)
    # build table
    df = pd.DataFrame(code_list)

    columns = ['Label', 'Code_Order', 'Code_Value', 'Category']
    df = df[columns]

    return df, codelist_dict


def get_response_table(root):
    """
    input: parsed 'SuestionItem'
    output: response table, question item and response label dictionary
    """
    QuestionItem_list = parse_QuestionItem(root)

    qi_response_dict = {}
    response_list = []
    for QuestionItem in QuestionItem_list:
        response_dict = {}

        if QuestionItem['TextDomain'] != {}:
            Label = QuestionItem['TextDomain']['Label']
            minLength = QuestionItem['TextDomain']['minLength']
            maxLength = QuestionItem['TextDomain']['maxLength']
            if Label == 'Generic text' and minLength == None and maxLength == None:
                Label = 'Generic text'
                minLength = 0
                maxLength = 255
            response_dict['Label'] = Label
            response_dict['Type'] = 'Text'
            response_dict['Type2'] = None
            response_dict['Format'] = None
            response_dict['Min'] = minLength
            response_dict['Max'] = maxLength
            response_list.append(response_dict)
            qi_response_dict[QuestionItem['QuestionItem_index']] = Label

        if QuestionItem['NumericDomain'] != {}:
            Label = QuestionItem['NumericDomain']['Label']
            low_value = QuestionItem['NumericDomain']['low_value']
            high_value = QuestionItem['NumericDomain']['high_value']
            if Label == 'Generic number' and high_value == None:
                Label = 'Long number'
            else:
                Label = 'Range: ' + str(low_value) + '-' + str(high_value)
            response_dict['Label'] = Label
            response_dict['Type'] = 'Numeric'
            response_dict['Type2'] = QuestionItem['NumericDomain']['NumericType']
            response_dict['Format'] = None
            response_dict['Min'] = low_value
            response_dict['Max'] = high_value
            response_list.append(response_dict)
            qi_response_dict[QuestionItem['QuestionItem_index']] = Label

        if QuestionItem['DateTimeDomain'] != {}:
            Type = QuestionItem['DateTimeDomain']['DateType']
            if Type == 'Date':
                Type2 = 'DateTime'
                Format = 'dd/mm/yyyy'
            else:
                Type2 = None
                Format = QuestionItem['DateTimeDomain']['Format']
            Label = 'Generic date'
            response_dict['Label'] = Label
            response_dict['Type'] = Type
            response_dict['Type2'] = Type2
            response_dict['Format'] = Format
            response_dict['Min'] = None
            response_dict['Max'] = None
            response_list.append(response_dict)
            qi_response_dict[QuestionItem['QuestionItem_index']] = Label 

    # build table
    df = pd.DataFrame(response_list)
    # remove duplicates
    df = df.drop_duplicates()

    cols = ['Label', 'Type' ,'Type2', 'Format', 'Min', 'Max']
    df = df[cols]

    return df, qi_response_dict


def get_condition_logic(root):
    """
    add logic to parsed IfThenElse
    """
    condition_list = parse_IfThenElse(root)

    # condition label based on first logic
    condition_label = None
    label_list = []

    for condition in condition_list:
        Literal = condition['IfCondition']['CommandContent'].replace('"','').replace("'",'')
        Literal = Literal.replace('Show the field ONLY if:', 'Display This Question:').replace('If If ', 'If ').replace(' Or Or ', 'Or ').replace(' And And ', 'And ')

        #TODO: this does not cover all cases

        # find text with the format "<label> <answer> Is Selected"
        Logic_parts = re.findall(r"[If|Or|And] (.*?) Is Selected", Literal)
        # format "If <label> Is Equal to <answer>"
        Logic_equal = re.findall(r"If (.*?) Is Equal to (.*)", Literal)
        # format "If <label> Is Empty"
        Logic_empty = re.findall(r"If (.*?) Is Empty", Literal)
        # format "If <label> Is Not Selected And <next label> Is Greater Than <answer>"
        Logic_complex = re.findall(r"[If|Or|And] (.*?) Is Not Selected And (.*?) Is Greater Than (.*)", Literal)
        # format "If <label> Is Selected And <next label> Is Equal to <answer>"
        Logic_comp_1 = re.findall(r"[If|Or|And] (.*?) Is Selected And (.*?) Is Equal to (.*)", Literal)
        # format "<label> <answer> Is Selected And <next label> <next answer> Is Not Selected"
        Logic_comp_2 = re.findall(r"If (.*?) Is Selected And (.*?) Is Not Selected", Literal)

        if len(Logic_comp_1) == 1:
            q = Logic_comp_1[0][0].split(' ')[0]
            Logic = 'qc_' + q + ' == ' + Logic_comp_1[0][0].replace(q, '') + ' && ' + Logic_comp_1[0][1] + ' == ' + Logic_comp_1[0][2]
            label = q.strip()

        elif len(Logic_comp_2) == 1:
            q1 = Logic_comp_2[0][0].split(' ')[0]
            q2 = Logic_comp_2[0][1].split(' ')[0]
            Logic = 'qc_' + q1 + ' == ' + Logic_comp_2[0][0].replace(q1, '') + ' && ' + 'qc_' + q2 + ' == ' + Logic_comp_2[0][1].replace(q2, '')
            label = q1.strip()

        elif len(Logic_complex) == 1:
            q = Logic_complex[0][0].split(' ')[0]
            Logic = 'qc_' + q + ' != ' + Logic_complex[0][0].replace(q, '') + ' && ' + Logic_complex[0][1] + ' > ' + Logic_complex[0][2]
            label = q.strip()

        elif len(Logic_equal) == 1:
            Logic = 'qc_' + Logic_equal[0][0].strip() + ' == ' + Logic_equal[0][1].strip()
            label = Logic_equal[0][0].strip()

        elif len(Logic_empty) == 1:
            label = Logic_empty[0].strip()
            Logic = 'qc_' + label + ' == empty'

        elif len(Logic_parts) > 0:

            Logic_words = re.findall("And|Or", Literal)
            replacements = {'AND': '&&', 'and': '&&', 'And': '&&', 'OR' : '||', 'or' : '||', 'Or' : '||'}
            replacer = replacements.get  # For faster gets.
            Logic_symbols = [replacer(n, n) for n in Logic_words]

            # check if it makes sense
            if len(Logic_words) + 1 != len(Logic_parts):
                print("Literal didn't split correctly: \n" + Literal )

            item_list = []
            for idx, item in enumerate(Logic_parts):
                if ' ' in item:
                    s =  item.split(' ', 1)
                    question = s[0]
                    answer = s[1]

                    if idx < len(Logic_parts) - 1:
                        item_new = 'qc_' + question + ' == ' + answer + ' ' + Logic_symbols[idx]
                    else:
                        item_new = 'qc_' + question + ' == ' + answer
                else:
                    item_new = 'qc_' + item

                item_list.append(item_new)
            Logic = ' '.join(item_list)
            #print('Logic: ' + Logic)
            label = Logic_parts[0].split(' ', 1)[0]
        else:
            print(Literal)
            print(Logic_parts)
            print(Logic_equal)
            Logic = Literal
            for ch in set(Logic_parts):
                if ch in Logic:
                    Logic = Logic.replace(ch, "qc_" + ch)
            Logic = Logic.replace('[', '').replace(']', '').replace('AND', '&&').replace('OR', '||').replace("'", '').replace('=', ' == ').replace('or', '||').replace('and', '&&')
            label = Logic_parts[0].split(' ', 1)[0]

        condition['Literal'] = 'Display This Question: ' + Literal
        condition['Logic'] = Logic
        condition['Condition_label'] = 'c_q' + label

    return condition_list


def get_position_w_if(root):
    """
    input: parsed IfThenElse (with logic), Sequence, QuestionConstruct
    output: question item and it's parent condition
    """
    condition_list = get_condition_logic(root)
    sequence_list = get_sequence_label(root)
    QC_list = parse_QuestionConstruct(root)
    QI_list = parse_QuestionItem(root)

    qi_con_list = []
    label_list = []
    for condition in condition_list:
        if_id = condition['ID']
        then_id = condition['ThenConstructReference']['ID']
        # these are all sequence type

        condition_label = condition['Condition_label']
        for seq in sequence_list:
            qi_con_dict = {}
            if seq['ID'] == then_id:
                seq_ref_list = seq['ControlConstructReference']

                for item in seq_ref_list:
                    ref_type = item['TypeOfObject']

                    # all questions construct?
                    if ref_type == 'QuestionConstruct':
                        ref_ID = item['ID']
                        i = 1
                        # which question
                        for QC in QC_list:
                            if QC['ID'] == ref_ID:

                                if QC['QuestionReference']['TypeOfObject'] == 'QuestionItem':
                                    QI_ID = QC['QuestionReference']['ID']
                                    qi_con_dict['Position'] = i
                                    qi_con_dict['parent_condition_ID'] = if_id

                                    # question item label
                                    for QI in QI_list:
                                        if QI['ID'] == QC['QuestionReference']['ID']:
                                            QI_Name = QI['Name']
                                            qi_con_dict['QI_ID'] = QI_ID
                                            qi_con_dict['QI_Name'] = QI_Name

                                            # condition label
                                            if condition_label is None:
                                                condition_label = 'c_q' + QI_Name
                                            # number of occurrences
                                            num_o = label_list.count(condition_label)
                                            if num_o > 0:
                                                new_label = condition_label + '_' + int_to_roman(num_o)
                                            else:
                                                new_label = condition_label

                                            qi_con_dict['parent_condition_Label'] = new_label

                                    label_list.append(condition_label)
                                    qi_con_list.append(qi_con_dict)

                    # TODO
                    else:
                        print('TODO: ' + ref_type)
    return qi_con_list


def get_condition_table(root):
    """
    input: parsed 'IfThenElse' and 'sequence'
    output: condition table
    """
    position_list = get_position_w_seq(root)
    seq_position_list = [item for item in position_list if item['TypeOfObject'] == 'IfThenElse']

    qi_if_position_list = get_position_w_if(root)

    condition_list = get_condition_logic(root)

    con_list = []
    for condition in condition_list:
        con_dict = {}
        Label = 'c_q' + str(condition['IfThenElse_index'] + 1)
        # using question item within the condition as label
        for item in qi_if_position_list:
            if item['parent_condition_ID'] == condition['ID']:
                Label = item['parent_condition_Label']

        con_dict['Label'] = Label
        con_dict['Literal'] = condition['Literal']
        con_dict['Logic'] = condition['Logic']

        parent_seq_list = [item for item in seq_position_list if item['ID'] == condition['ID']]
        if len(parent_seq_list) == 1:
            parent_seq = parent_seq_list[0]
            con_dict['Parent_Type'] = 'CcSequence'
            con_dict['Parent_Name'] = parent_seq['Parent_Seq_Label']
            con_dict['Position'] = parent_seq['Position']

        con_list.append(con_dict)
    # build table
    df = pd.DataFrame(con_list)
    df['Branch'] = None

    cols = ['Label', 'Literal', 'Logic', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df = df[cols]

    return df


def get_question_item_table(root):
    """
    input: parsed 'QuestionItem'/'QuestionConstruct', QuestionItem and it's response, sequence/condition position
    output: question item table
    """
    # question item's response
    df_response, qi_response_dict = get_response_table(root)
    df_codelist, qi_codelist_dict =  get_codelist_table(root)

    position_list = get_position_w_seq(root)
    seq_position_list = [item for item in position_list if item['TypeOfObject'] == 'QuestionConstruct']

    qi_if_position_list = get_position_w_if(root)

    qi_all = parse_QuestionItem(root)
    qc_all = parse_QuestionConstruct(root)

    qi_list = []
    statement_id_list = []
    for qi in qi_all:
        qi_dict = {}
        qi_dict['Label'] = 'qi_' + qi['Name']
        qi_dict['Literal'] = clean_Literal(qi['Literal'])

        qi_dict['Instructions'] = qi['Instructions']
        if qi['QuestionItem_index'] in qi_response_dict.keys():
            qi_dict['Response'] = qi_response_dict[qi['QuestionItem_index']]
        elif qi['CodeDomain'] != {}:
            qi_dict['Response'] = qi_codelist_dict[qi['CodeDomain']['CodeListReference']['ID']]
        else:
            qi_dict['Response'] = None

        qi_dict['min_responses'] = qi['minimumResponses']
        qi_dict['max_responses'] = qi['maximumResponses']

        # how many non-empty response domain
        num_response = len([i for i in  list([qi['TextDomain'], qi['NumericDomain'], qi['DateTimeDomain'], qi['CodeDomain']]) if i != {}])

        if num_response == 1:
            rd_order = 1
        elif num_response == 0:
            # number of response == 0, move this to be a statement
            statement_id_list.append(qi_dict['Label'])
            rd_order = None
        else: #TODO 2 answers need to write out tow lines
            print('number of response == ' + num_response)
        qi_dict['rd_order'] = rd_order
        #TODO: how to identify interviewee?
        qi_dict['Interviewee'] = 'Cohort/sample member'

        Parent_Type = None
        Parent_Name = None
        Branch = None
        Position = None

        for qc in qc_all:
            if qc['QuestionReference']['ID'] == qi['ID']:
                for seq_pos in seq_position_list:
                    if seq_pos['ID'] == qc['ID']:
                        Parent_Type = 'CcSequence'
                        Parent_Name = seq_pos['Parent_Seq_Label']
                        Position = seq_pos['Position']

        for if_pos in qi_if_position_list:
            if if_pos['QI_ID'] == qi['ID']:
                Parent_Type = 'CcCondition'
                Parent_Name = if_pos['parent_condition_Label']
                Branch = 0
                Position = if_pos['Position']

        qi_dict['Parent_Type'] = Parent_Type
        qi_dict['Parent_Name'] = Parent_Name
        qi_dict['Branch'] = Branch
        qi_dict['Position'] = Position

        qi_list.append(qi_dict)
    # build table
    df = pd.DataFrame(qi_list)

    # column type is integer
    df[['Position', 'Branch', 'rd_order']] = df[['Position', 'Branch', 'rd_order']].astype('Int64')

    cols = ['Label', 'Literal', 'Instructions', 'Response', 'Parent_Type', 'Parent_Name', 'Branch', 'Position', 'min_responses', 'max_responses', 'rd_order', 'Interviewee']
    df = df[cols]

    # seperate statements
    df_statement = df[df['Label'].isin(statement_id_list)]
    df_question = df[~df['Label'].isin(statement_id_list)]

    df_statement['Label'] = df_statement['Label'].apply(lambda x: x.replace('qi_', 's_'))
    df_statement = df_statement[['Label', 'Literal', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']]

    return df_question, df_response, df_codelist, df_statement


def main():
    output_dir = 'archivist_tables'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    tree = ET.parse(xmlFile, parser=ET.XMLParser(encoding="utf-8"))
    root = tree.getroot()

    # inspect xml
    section_names = set([i.tag for i in root.findall('./ddi:Fragment/', ns)])
    for section_name in section_names:
        print("****** {} ******".format(section_name))

    df_sequence = get_sequence_table(root)
    df_sequence.to_csv(os.path.join(output_dir, 'sequence.csv'), index=False, sep='\t')

    df_condition = get_condition_table(root)
    df_condition.to_csv(os.path.join(output_dir, 'condition.csv'), index=False, sep='\t')

    df_QI, df_response, df_codelist, df_st = get_question_item_table(root)
    df_QI.to_csv(os.path.join(output_dir, 'question_item.csv'), index=False, sep='\t')
    df_response.to_csv(os.path.join(output_dir, 'response.csv'), index=False, sep='\t')
    df_codelist.to_csv(os.path.join(output_dir, 'codelist.csv'), index=False, sep='\t')

    df_statement = get_statement_table(root)
    df_statement_out = df_statement.append(df_st)
    df_statement_out.to_csv(os.path.join(output_dir, 'statement.csv'), index=False, sep='\t')


if __name__ == "__main__":
    main()
